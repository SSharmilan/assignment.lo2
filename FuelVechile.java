
public class FuelVechile extends RentedVechile {
	 private double nbkms;

	    public FuelVechile(double basefees, double nbkms) {
	        super(basefees);
	        this.nbkms = nbkms;
	        this.basefees = basefees;
	        // TODO Auto-generated constructor stub
	    }

	    public double getmilleage() {


	        if (nbkms < 100) {
	            return 0.2 * nbkms;
	        } else if (nbkms >= 100 && nbkms <= 400) {
	            return 0.3 * nbkms;
	        } else {
	            return (0.3 * 400) + (nbkms - 400) * 0.5;
	        }

	    }

}
